# Mail 2 Octoprint

Polls mail accounts and uploads gcode file attachments to octoprint

## Setup

Install via the bundled [Plugin Manager](https://docs.octoprint.org/en/master/bundledplugins/pluginmanager.html)
or manually using this URL:

    https://gitlab.com/hartmnt/mailtooctoprint/-/archive/master/mailtooctoprint-master.zip


## Usage

1) Fill in your respective IMAP settings.  

2) This plugin polls the inbox of a mail account regulary.

3) Mails with the subject ``octoprint`` are selected. Their attachments are automatically imported into octoprint, if the MIME-type is detected as ``text/x.gcode``.
