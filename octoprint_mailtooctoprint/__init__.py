# coding=utf-8
from __future__ import absolute_import

import octoprint.plugin
import octoprint.filemanager
from octoprint.filemanager.destinations import FileDestinations
from octoprint.util import RepeatedTimer

import io
import imaplib, email
from email import policy
import datetime

class MailToOctoprintPlugin(octoprint.plugin.StartupPlugin,
                      octoprint.plugin.TemplatePlugin,
                      octoprint.plugin.SettingsPlugin):

	def get_settings_defaults(self):
		return dict(imap_host="", imap_user="", imap_password="", wait_minutes=5)

	def get_template_configs(self):
		return [
			dict(type="settings", custom_bindings=False)
		]

	def on_after_startup(self):
		self._logger.info("Initializing MailToOctoprint plugin!")

		repeatMinutes = max(1, int(self._settings.get(["wait_minutes"])))
		self._logger.info("Checking for new mail every %i minute(s)!" % (repeatMinutes))
		self.repeated_timer = RepeatedTimer(repeatMinutes * 60, self.update_mails)
		self.repeated_timer.start()

		# Additionally check for mails immediately
		self.update_mails()

	def update_mails(self):
		self._logger.debug("Checking for new mail!")

		imaphost = self._settings.get(["imap_host"])
		imapuser = self._settings.get(["imap_user"])
		imappassword = self._settings.get(["imap_password"])

		if imaphost == "" or imapuser == "" or imappassword == "":
			self._logger.info("Incomplete configuration!")
			return

		mail = imaplib.IMAP4_SSL(imaphost, 993)
		rc, resp = mail.login(imapuser, imappassword)

		mail.select('Inbox')
		status, data = mail.search(None, '(UNSEEN)', '(SUBJECT "octoprint")')

		imported_files = 0
		imported_mails = 0
		for num in data[0].split():

			status, data = mail.fetch(num, '(RFC822)')
			email_msg = data[0][1]
			email_msg = email.message_from_bytes(email_msg, policy=policy.SMTP)

			all_attachments = email_msg.get_payload()

			if len(all_attachments) < 2:
				continue

			imported_mails += 1
			for attachment in all_attachments:

				if attachment.is_attachment():
					imported_files += 1
					payload = attachment.get_payload(decode=True)

					file_name = "mailimport_%s_%s" % (datetime.datetime.now().strftime("%Y%m%d_%H:%M:%S"), attachment.get_filename())

					canonPath, canonFilename = self._file_manager.canonicalize(FileDestinations.LOCAL, file_name)
					futurePath = self._file_manager.sanitize_path(FileDestinations.LOCAL, canonPath)
					futureFilename = self._file_manager.sanitize_name(FileDestinations.LOCAL, canonFilename)
					futureFullPath = self._file_manager.join_path(FileDestinations.LOCAL, futurePath, futureFilename)
					futureFullPathInStorage = self._file_manager.path_in_storage(FileDestinations.LOCAL, futureFullPath)
					stream = octoprint.filemanager.util.StreamWrapper(file_name, io.BytesIO(payload))
					self._file_manager.add_file(octoprint.filemanager.FileDestinations.LOCAL, futureFullPathInStorage, stream, allow_overwrite=False, display=attachment.get_filename())

		if len(data[0]) > 0:
			self._logger.info("Received %i new mails with %i new attachment!" % (imported_mails, imported_files))
		else:
			self._logger.debug("No new mails!")


__plugin_name__ = "Mail 2 Octoprint"
__plugin_description__ = "Send GCode directly to Octoprint via EMail"
__plugin_pythoncompat__ = ">=3,<4"


def __plugin_load__():
	global __plugin_implementation__
	__plugin_implementation__ = MailToOctoprintPlugin()
